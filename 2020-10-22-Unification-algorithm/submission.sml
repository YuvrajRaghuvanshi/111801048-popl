signature SIG = sig
    type symbol
    val arity: symbol->int
    structure Ord: ORD_KEY where type ord_key = symbol
end;

signature VAR = sig
    type var
    structure Ord: ORD_KEY where type ord_key = var
end;


functor Term (S: SIG) (V: VAR) = struct

(*constants are just considered as functions with arity 0 so TS captures both constants and functions in S*)
datatype term = TV of V.var | TS of (S.symbol*term list)
fun occurs (t: term, v: V.var): bool =
    case t of
	TV x => if (V.Ord.compare (v,x)) = EQUAL then true else false
      | TS (f, xs) => let fun listcheck (lst: term list) =
			      case lst of
				  l::ls => if occurs (l, v) then true else listcheck(ls)
				| [] => false
		      in listcheck(xs)
		      end

(*create a valid function application term*)			  
			  
fun app (x: S.symbol, lst: term list): term = let exception INVALID in if (S.arity x = List.length lst) then (TS (x, lst)) else raise INVALID end

(* create a variable term *)
						  
fun variable (x: V.var) = TV x						  
			     
end
				     
functor Telescope (S: SIG) (V:VAR) = struct

structure trm = Term (S) (V)
		     
(*telescope is implemented as a red black map*)
structure telescope = RedBlackMapFn(V.Ord)

type map = trm.term telescope.map
type term = trm.term
val empty = telescope.empty
		

fun checkinsert (tele: map) (x: V.var) (trm.TV (v)): bool = (if (V.Ord.compare(x, v) = EQUAL) then false else (let val value = telescope.find(tele, v) in
														   (case value of
															SOME t => checkinsert tele x t 
														      | NONE => true ) end))
  | checkinsert (tele: map) (x: V.var) (trm.TS (sym, args)) = (let fun listcheck (lst: term list) =
								       case lst of
									   l::ls => if (checkinsert tele x l) then listcheck ls else false
									 | [] => true in
								   listcheck args end) 

fun insert (tele: map) (x: V.var) (t: term): map option =
    case t of
	trm.TV v => if V.Ord.compare(x, v) = EQUAL then SOME tele else (if (checkinsert tele x t) then SOME (telescope.insert(tele, x, t)) else NONE)
      | _ => (if (checkinsert tele x t) then SOME (telescope.insert(tele, x, t)) else NONE)

end;



functor Unification (S: SIG) (V:VAR) = struct

structure tlscp = Telescope (S) (V)
structure trm = tlscp.trm

fun combineLists (lst1) (lst2) =
    case (lst1, lst2) of
	([], []) => []
      | ([], y) => []
      | (x, []) => []
      | (x::xs, y::ys) => (x, y) :: (combineLists xs ys)
					
fun unify (tele: tlscp.map) (s: trm.term, t: trm.term): tlscp.map option =
    case (s, t) of
	(trm.TV x, trm.TS (name, args)) => (case tlscp.telescope.find(tele, x) of
						NONE => tlscp.insert tele x t
					      | SOME newterm => unify tele (newterm, t))
      | (trm.TV x, trm.TV y) => (case (tlscp.telescope.find(tele, x), tlscp.telescope.find(tele, y)) of
				     (NONE, NONE) => (tlscp.insert tele x t)
				   | (SOME a, SOME b) => (unify tele (a, b))
				   | (NONE, SOME b) => (tlscp.insert tele x b)
				   | (SOME a, NONE) => (tlscp.insert tele y a))
      | (trm.TS(name, args), trm.TV x) => (unify tele (t, s))
      | (trm.TS(name1, args1), trm.TS(name2, args2)) => if (S.arity name1) = (S.arity name2) then (unifyList tele (combineLists args1 args2)) else NONE

and unifyList (tele: tlscp.map) (lst: (trm.term*trm.term) list): tlscp.map option =
    case lst of
	[] => SOME tele
      | l::ls => let val tele2 = (unify tele l) in
		     case tele2 of
			 NONE => NONE 
		       | SOME t =>  (unifyList t ls) end
end;



(* Small example for testing above code. Uncomment to run *)

(*

(* Three symbols c (constant), fa (arity = 1), and fb (arity = 2) *)
datatype symbols = c | fa | fb

(*three variables x1, x2, x3 *)
datatype vars = x1 | x2 | x3

structure S: SIG = struct
type symbol = symbols
fun arity c = 0
  | arity fa = 1
  | arity fb = 2
structure Ord: ORD_KEY = struct
type ord_key = symbol
fun compare (c, fa) = LESS
  | compare (fa, c) = GREATER
  | compare (c, c) = EQUAL
  | compare (fa, fa) = EQUAL
  | compare (fa, fb) = LESS
  | compare (fb, fa) = GREATER
  | compare (fb, fb) = EQUAL
  | compare (fb, c) = GREATER
  | compare (c, fb) = LESS
end
end

structure V: VAR = struct
type var = vars
structure Ord: ORD_KEY = struct
type ord_key = var
fun compare (x1, x3) = LESS
  | compare (x1, x2) = LESS
  | compare (x1, x1) = EQUAL
  | compare (x2, x1) = GREATER
  | compare (x2, x2) = EQUAL
  | compare (x2, x3) = GREATER
  | compare (x3, x1) = GREATER
  | compare (x3, x2) = LESS
  | compare (x3, x3) = EQUAL
end
end
		       
structure UNF = Unification (S) (V)
val map = UNF.tlscp.empty

(* bind t1 = fb(x1, fb(x2, x1))*)
val t1 = UNF.trm.app (fb, [UNF.trm.variable x1, UNF.trm.app (fb, [UNF.trm.variable x2, UNF.trm.variable x1])])

(* bind t2 = fb(fa(x2), fb(fa(c), fa(x3))) *)
val t2 = UNF.trm.app(fb, [UNF.trm.app (fa, [UNF.trm.variable x2]), UNF.trm.app (fb, [UNF.trm.app(fa, [UNF.trm.app(c, [])]), UNF.trm.app(fa, [UNF.trm.variable x3])])])
		    
(* unify t1 and t2 *)
val unified = UNF.unify map (t1, t2);

(* Increase print depth to see maps in smlnj*)
Control.Print.printDepth := 60;

(* check answers *)
(* should be x1 = fa(x2) *)
val X1 = UNF.tlscp.telescope.find(valOf(unified), x1);

(* should be x2 = fa(c) *)
val X2 = UNF.tlscp.telescope.find(valOf(unified), x2);

(* should be x3 = x2 = fa(c) *)
val X3 = UNF.tlscp.telescope.find(valOf(unified), x3);

*)
