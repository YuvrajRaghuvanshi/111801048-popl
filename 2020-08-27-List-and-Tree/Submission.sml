(*Q1.*)
fun map (f: 'a -> 'b)  (a:'a list): 'b list =
    case a of
	[] => []
      | x::xs => f x :: map f xs;

(*
Testing map function:

fun add a b = a+b;

val incr = add 1;

val outputlist = map incr [4,2,5,6,12,45,87];
*)

(*Q2.*)

datatype 'a tree = Empty | Node of 'a*'a tree*'a tree;

(*Q3.*)

fun treemap (f: 'a -> 'b) (inptree:'a tree): 'b tree =
    case inptree of
	Empty => Empty
      | Node(data,left,right) => Node(f data, treemap f left, treemap f right);


(*
Declarations and evaluations for testing treemap

val someTree = Node(5, Node(2, Node(1, Empty, Empty), Node(4, Empty, Empty)), Node(7, Node(6, Empty, Empty), Empty));
someTree = 5
          / \
         /   \
        2     7
       / \   / 
      1   4  6

val mappedTree = treemap incr someTree;
*)

(*Q4.*)

fun preorder (intree: 'a tree): 'a list =
    case intree of
	Empty => []
      | Node(data,left,right) => data :: preorder left @ preorder right;

fun inorder (intree: 'a tree): 'a list =
    case intree of
	Empty => []
      | Node(data,left,right) => inorder left @ [data] @ inorder right;

fun postorder (intree: 'a tree): 'a list =
    case intree of
	Empty => []
      | Node(data,left,right) => postorder left @ postorder right @ [data];

(*Q5.*)

fun rotateCW (inptree: 'a tree): 'a tree =
    case inptree of
	Empty => Empty
      | Node(data, Node(datal1, leftl1, rightl1), right) => Node(datal1, leftl1, Node(data, rightl1, right))
      | Node(data, Empty, _) => (print("Empty left child. Rotation not possible."); inptree);
(*
Testing out the rotate function:

val rotatedTree = rotateCW(someTree);
val it = inorder rotatedTree;
*)
