(* Q1 *)

type var = string;
datatype expr = var of var | lambda of var*expr | app of expr*expr;

(*returns a string of maximum length from the string list*)
fun maxstring (lst: string list): string =
    case lst of
	[] => ""
      | x::xs => let val rest = maxstring xs in if String.size x > String.size (maxstring xs) then x else rest end;

(*returns a string list same as input string list with an element deleted*)
fun delete (str: string) (lst: string list): string list =
    case lst of
	[] => []
      | x::xs => let val rest = delete str xs in if x = str then rest else x::rest end;

(* couldnt figure out how to use Cantor's diagonalization to generate unique string so using the approach of appending 'a' at the end of the longest string in the list *)
(* Q2 *)

fun fresh (lst :string list): string = maxstring lst ^ "a";

(* Q3 *)

fun free (inp: expr): var list =
    case inp of
	var variable => [variable]
      | lambda (variable, expression)  => delete variable (free expression)
      | app (expr1,expr2) => free expr1 @ free expr2;

(* helper functions *)

fun first (inp: expr): expr =
    case inp of
	app(expr1, expr2) => expr1
      | lambda(arg, body) => var(arg)
      | var(variable) => inp
			     
fun second (inp: expr): expr =
    case inp of
	app(expr1, expr2) => expr2
      | lambda(arg, body) => body
      | var(variable) => inp

(* Q4 *)

fun subst (x: var, N: expr) (M: expr): expr =
    case M of
	app (expr1, expr2) => app (subst (x, N) expr1, subst (x, N) expr2)
      | lambda (arg, body)  => if arg = x then lambda (arg, body) else lambda (arg, subst (x, N) body )
      | var(variable) => if variable = x then N else M;

(* Bonus Question *)

fun variables (e:expr): var list =
    case e of
	var(v) => [v]
      | lambda(arg, body) => arg :: variables body
      | app(expr1,expr2) => variables expr1 @ variables expr2;

fun freshen (vs: var list) (e: expr): expr =
    let val vss = variables e @ vs in
	case e of
	    var(_) => e
	  | lambda(arg, body) => let val newarg = fresh vss in
				     lambda(newarg, freshen vss (subst (arg, var(newarg)) body ))
				 end
	  | app (expr1,expr2) => app(freshen vs expr1, freshen vs expr2)
    end;

fun argument (lambda(x, _): expr): var = x
  | argument (_:expr): var = "";

fun betareduce (e: expr): expr =
    case e of
	var(_) => e
      | lambda(arg, body) => lambda(arg, betareduce body)
      | app(lambda(arg, body), expr2) => betareduce(let  val e2 = freshen (variables expr2) (lambda(arg, body)) in subst (argument e2, expr2) (second e2) end)
      | app(var _, var _) => e
      | app(expr1, expr2) =>  betareduce (app(betareduce expr1, betareduce expr2));

(*The lambda expression (fn x => x)(fn y => y x)*)
val exp: expr = app(lambda ("x", var "x"), lambda("y", app(var "y", var "x")));



