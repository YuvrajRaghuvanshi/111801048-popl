(* Ex 1. *)

fun curry (f:'a*'b*'c->'d) (a:'a) (b:'b) (c:'c) = f(a,b,c);
fun uncurry (f:'a->'b->'c->'d) (a:'a, b:'b, c:'c) = f a b c;

(*Ex 2.*)

fun fst (a:'a, b:'b) = a;
fun snd (a:'a, b:'b) = b;

(*Ex 3.*)

fun length (lst: 'a list): int =
    case lst of
	[] => 0
      | x::sublst => 1 + length(sublst);

(*Ex 4.*)

fun reverse (inplst: 'a list): 'a list =
    case inplst of
	[] => [] 
      | x::sublst => reverse(sublst) @ [x];


(*Ex 5.*)

fun getfib (n: int, prev2:int, prev1:int): int =
    case n of
	2 => prev1 + prev2
      | _ => getfib(n-1, prev1, prev1+prev2);

fun fib (n:int):int =
    if n < 45 then getfib(n,0,1)
    else (print("45th and above terms too large to fit in int\n"); ~1);
