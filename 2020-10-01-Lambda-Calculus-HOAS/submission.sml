type var = string

datatype lam = V of var | A of lam*lam | L of var*lam

datatype hlam = HV of var | HA of hlam*hlam | HL of hlam -> hlam

(* Q1. *)
								
fun subst (x: var, N: hlam) (e: hlam): hlam =
    case e of
	HV (v) => if v = x then N else e
      | HA (e1, e2) => HA (subst (x, N) e1, subst (x, N) e2)
      | HL (f) => HL (fn t => (subst (x, N) (f t)))

(* Q2. *)
		     
fun abstract (x: var) (mh: hlam): hlam = HL (fn (nh: hlam) => subst (x, nh) mh)

(*returns a string of maximum length from the string list*)
fun maxstring (lst: string list): string =
    case lst of
	[] => ""
      | x::xs => let val rest = maxstring xs in if String.size x > String.size (maxstring xs) then x else rest end;

fun fresh (lst :string list): string = maxstring lst ^ "a"

(* Q3. *)

fun freeP (e: hlam): string list =
    case e of
	HV (v) => [v]
      | HA (e1, e2) => (freeP e1) @ (freeP e2)
      | HL f => freeP (f (HV "x"))
		      
		      
fun freshen (e: hlam): string = fresh (freeP e)

(* Q4. *)
				      
fun hoas (e: lam): hlam =
    case e of
	V (x) => HV (x)
      | A (e1, e2) => HA (hoas e1, hoas e2)
      | L (arg, body) => abstract arg (hoas body)

fun syntax (e: hlam): lam =
    case e of
	HV (x) => V (x)
      | HA (e1, e2) => A (syntax e1, syntax e2)
      | HL f => let val a = freshen e in L (a, syntax (f (HV a))) end


(* Functions and expressions for testing purposes. Not part of the assignment*)

fun first (HL f) = f (HV "x")

val hexpr = HA(HL (fn t => t), HV "x")
val lexpr = A(L ("t", V "t"), V "x")

