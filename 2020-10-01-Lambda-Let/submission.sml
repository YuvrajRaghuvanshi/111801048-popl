type var = string;

datatype expr =  VAR of var | LAMBDA of var*expr | APP of expr*expr;


datatype letexpr = VARL of var | LAMBDAL of var*letexpr | APPL of letexpr*letexpr | LET of var*letexpr*letexpr;


datatype letrecexpr = VARLR of var | LAMBDALR of var*letrecexpr | APPLR of letrecexpr*letrecexpr | LETLR of var*letrecexpr*letrecexpr | LETREC of var*letrecexpr*letrecexpr;

fun Y (f:letrecexpr): letrecexpr =
    case f of
	LAMBDALR (arg, body) => APPLR(LAMBDALR("x", APPLR(f, APPLR(VARLR "x", VARLR "x"))), LAMBDALR("x", APPLR(f, APPLR(VARLR "x", VARLR "x"))))
     | _ => f

fun unlet (e: letexpr): expr =
    case e of
	VARL (x) => VAR(x)
     | LAMBDAL (arg, body) => LAMBDA (arg, unlet body)
     | APPL(expr1, expr2) => APP(unlet expr1, unlet expr2)
     | LET (arg, sub, body) => APP (LAMBDA(arg, unlet body), unlet sub);

fun unletrec (e:letrecexpr): letexpr =
    case e of
	VARLR (x) => VARL (x)
     | LAMBDALR (arg, body) => LAMBDAL (arg, unletrec body)
     | APPLR (expr1, expr2) => APPL(unletrec expr1, unletrec expr2)
     | LETLR (arg, sub, body) => LET (arg, unletrec sub, unletrec body)
     | LETREC (arg, sub, body) => LET (arg, unletrec (Y (LAMBDALR(arg, sub))), unletrec body);

