signature COUNTER = sig
    type counter
    val incr: unit->unit
    val decrement: unit->unit
    val get: unit->int
end
			

structure Counter: COUNTER = struct
    type counter = int
    val cnt: counter ref = ref 0
    fun incr () = cnt := !cnt + 1
    fun decrement () = cnt := !cnt - 1
    fun get ():int = !cnt
end



functor MkCounter (): COUNTER = Counter



structure A = MkCounter ()
structure B = MkCounter ()


(*Bonus Question*)
(* Incorrect implementation. Does not work *)			
structure atomCnt = MkCounter ()

structure i2skey = struct
    type ord_key = int
    val compare = Int.compare
end

structure s2ikey = struct
    type ord_key = string
    val compare = String.compare
end

structure I2Smap = RedBlackMapFn(i2skey)
structure S2Imap = RedBlackMapFn(s2ikey)			     
			      
signature ATOM = sig
    type atom
    val s2i : string -> int option
    val i2s : int -> string option
end

val I2S = ref I2Smap.empty
val S2I = ref S2Imap.empty     
		     
structure Atom: ATOM = struct
type atom = int
fun s2i (str: s2ikey.ord_key):int option = if S2Imap.find(!S2I, str) = NONE then (S2Imap.insert (!S2I, str, atomCnt.get()); I2Smap.insert (!I2S, atomCnt.get(), str); atomCnt.incr(); NONE)
else S2Imap.find(!I2S, str)			

fun i2s (i: i2skey.ord_key):string option = I2Smap.find(!I2S, i)
end		       
		       
