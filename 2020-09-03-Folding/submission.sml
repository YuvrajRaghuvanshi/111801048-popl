(*Q1.*)

fun foldl (sfun: 'a*'b -> 'b) (s0: 'b) (lst: 'a list):'b=
    case lst of
	[] => s0
      | x::xs => foldl sfun (sfun(x,s0)) xs;


fun foldr (sfun: 'a*'b-> 'b) (s0: 'b) (lst: 'a list): 'b =
    case lst of
	[] => s0
      | x::xs => sfun(x, foldr sfun s0 xs);

(*Q2.*)

fun plus (a:int,b:int):int = a+b; (* plus will be used as sfun for testing purposes *)
fun mul (a:int,b:int):int = a*b; (*mul will also be used as sfun for testing*)

(* sum using foldl. foldl should be preferred over foldr for large lists because it's tail recursive and hence faster than foldr *) 

fun sum (lst: int list):int = foldl plus 0 lst;

(*Q3.*)

fun incr (a: int):int = a+1;

fun isEven (a:int):bool = a mod 2 = 0

fun partition (f:'a->bool) (lst: 'a list):('a list*'a list) = foldr (fn (x:'a,(lst1: 'a list, lst2: 'a list)) => if (f x) = true then (x::lst1, lst2) else (lst1, x::lst2)) ([],[]) lst;

fun map (f:'a->'b) (lst: 'a list):('b list) = foldr (fn (x:'a, xs:'b list) => (f x)::xs) [] lst;

fun reverse (lst:'a list):('a list)= foldl (fn (x:'a, xs:'a list) => x::xs) [] lst; 

datatype 'a Find = LookingFor of int | Found of 'a;

(* nthAux to act as sfun for fold *)
fun nthAux (elem: 'a, x:'a Find): 'a Find =
    case x of
	LookingFor i => if i = 0 then Found elem else LookingFor (i-1)
      | _ => x

fun nth (lst:'a list, i: int): 'a option =
    case foldl nthAux (LookingFor i) lst of
	LookingFor x => NONE
      | Found x => SOME x;
